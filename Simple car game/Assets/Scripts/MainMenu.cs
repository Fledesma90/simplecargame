using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
//namespace to use DateTime
using System;
public class MainMenu : MonoBehaviour
{
    [SerializeField] TMP_Text highScoreText;
    [SerializeField] TMP_Text energyText;
    [SerializeField] Button playButton;
    [SerializeField] IOSNotificationHandler IOSNotificationHandler;
    [SerializeField] int maxEnergy;
    //minutes or time to be able to come back to play
    [SerializeField] int energyRechargeDuration;

    int energy;

    const string EnergyKey = "Energy";
    const string EnergyReadyKey = "EnergyReady";

    private void Start() {
        OnApplicationFocus(true);
    }

    private void OnApplicationFocus(bool hasFocus) {
        if (!hasFocus) {
            return;
        }

        CancelInvoke();

        //default value if we cannot find anything it is 0
        int highScore=PlayerPrefs.GetInt(ScoreHandler.HighScoreKey, 0);
        highScoreText.text = $"HighScore: {highScore}";

        energy = PlayerPrefs.GetInt(EnergyKey,maxEnergy);

        if (energy==0) {
            string energyReadyString = PlayerPrefs.GetString(EnergyReadyKey, string.Empty);
            if (energyReadyString==string.Empty) {
                return;
            }
            DateTime energyReady = DateTime.Parse(energyReadyString);
            if (DateTime.Now>energyReady) {
                energy = maxEnergy;
                PlayerPrefs.SetInt(EnergyKey, energy);
            } else {
                playButton.interactable = false;
                Invoke(nameof(EnergyRecharged), (energyReady - DateTime.Now).Seconds);
            }
        }
        //Show on play button the remainder energy and the text Play
        energyText.text = $"Play({energy})";
    }

    void EnergyRecharged() {
        playButton.interactable=true;
        energy = maxEnergy;
        PlayerPrefs.SetInt(EnergyKey, energy);
        energyText.text = $"Play({energy})";
    }
    public void PlayButton() {
        if (energy<1) {
            return;
        }
        energy--;

        PlayerPrefs.SetInt(EnergyKey, energy);

        if (energy==0) {
            DateTime energyReady = DateTime.Now.AddMinutes(energyRechargeDuration);
            PlayerPrefs.SetString(EnergyReadyKey, energyReady.ToString());
            //the notification will be triggered after the time energyRechargeDuration
#if UNITY_IOS
            IOSNotificationHandler.ScheduleNotification(energyRechargeDuration);
#endif
        }

        SceneManager.LoadScene(1);
    }
}
