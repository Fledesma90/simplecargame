using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ScoreHandler : MonoBehaviour
{
    [SerializeField] TMP_Text scoreText;
    [SerializeField] float scoreMultiplier;
    float score;
    //const means this variable value cannot be changed
    public const string HighScoreKey = "HighScore";

    // Update is called once per frame
    void Update()
    {
        score += Time.deltaTime*scoreMultiplier;

        scoreText.text = Mathf.FloorToInt(score).ToString();
    }

    private void OnDestroy() {
       int currentHighScore = PlayerPrefs.GetInt(HighScoreKey, 0);
        if (score>currentHighScore) {
            PlayerPrefs.SetInt(HighScoreKey, Mathf.FloorToInt(score));
        }
    }
}
