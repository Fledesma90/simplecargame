using System.Collections;
using System.Collections.Generic;
#if UNITY_IOS
using Unity.Notifications.iOS;
#endif
using UnityEngine;

public class IOSNotificationHandler : MonoBehaviour
{
#if UNITY_IOS
    public void ScheduleNotification(int minutes) {
        iOSNotification notification = new iOSNotification {
            Title = "Energy Recharged",
            Subtitle = "Your energy has been recharged",
            Body ="Your energy has been recharged, come back to play again",
            ShowInForeground=true,
            //an alert pops up and a sound
            ForegroundPresentationOption=(PresentationOption.Alert | PresentationOption.Sound),
            CategoryIdentifier="category_a",
            ThreadIdentifier="thread1",
            //notification will show up after an amount of time has passed
            Trigger= new iOSNotificationTimeIntervalTrigger {
                //0hours, variable of minutes and 0 seconds
                TimeInterval= new System.TimeSpan(0, minutes, 0),
                //if we do not put this, notification will repeat every minute or time set
                Repeats=false
                
            }
        };

        iOSNotificationCenter.ScheduleNotification(notification);
    }
#endif
}
